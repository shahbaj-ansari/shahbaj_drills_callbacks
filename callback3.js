/* 
	Problem 3: Write a function that will return all cards that belong to a particular list based on the listID
    that is passed to it from the given data in cards.json. Then pass control back to the code that called it
    by using a callback function.
*/

const fs = require("fs");
const path = require("path");

function getAllCardsForAList(cardsPath, listID, callback) {
	if (typeof cardsPath != "string" || typeof listID != "string" || typeof callback != "function") {
		throw new Error(
			`Please Provide all 3 parameters in correct form : (cardsPath:string, cardID:string, callback:function)`
		);
	} else {
		setTimeout(() => {
			fs.readFile(path.join(__dirname, cardsPath), "utf-8", (err, data) => {
				if (err) {
					throw err;
				}

				try {
					const cards = JSON.parse(data);
					let result;
					for (list_id in cards) {
						if (listID == list_id) {
							result = cards[list_id];
							break;
						}
					}

					if (result == undefined) {
						throw new Error(`No cards found for given listID: ${listID}`);
					} else {
						callback(null, result);
					}
				} catch (error) {
					callback(error);
				}
			});
		}, 2 * 1000);
	}
}

module.exports = getAllCardsForAList;
