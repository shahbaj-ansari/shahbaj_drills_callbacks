/* 
	Problem 2: Write a function that will return all lists that belong to a board based on the boardID
    that is passed to it from the given data in lists.json. Then pass control back to the code that called
    it by using a callback function.
*/

const fs = require("fs");
const path = require("path");

function getAllListsOfABoard(listsPath, boardID, callback) {
	if (arguments.length < 3) {
		throw new Error("Please Provide all 3 parameters: (listPath, boardID, callback)");
	} else if (typeof listsPath != "string" || typeof boardID != "string" || typeof callback != "function") {
		throw new Error(
			"Please Provide all 3 parameters in correct form : (listsPath:string, boardID:string, callback:function)"
		);
	} else {
		setTimeout(() => {
			fs.readFile(path.join(__dirname, listsPath), "utf-8", (err, data) => {
				if (err) {
					throw err;
				}

				try {
					const lists = JSON.parse(data);
					const result = lists[boardID];

					if (result == undefined) {
						throw new Error(`No list found for given boardID : ${boardID}`);
					} else {
						callback(null, result);
					}
				} catch (error) {
					callback(error);
				}
			});
		}, 2 * 1000);
	}
}

module.exports = getAllListsOfABoard;
