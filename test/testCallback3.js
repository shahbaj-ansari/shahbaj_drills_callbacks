const getAllCardsForAList = require("../callback3.js");

function printCard(cards) {
	for (let i = 0; i < cards.length; i++) {
		setTimeout(() => console.log(cards[i]), (i + 1) * 1000);
	}
}

const callback = function (err, data) {
	if (err) {
		console.log(err);
	} else {
		printCard(data);
	}
};

// correct cards, correct cardID
try {
	getAllCardsForAList("data/cards.json", "qwsa221", callback);
} catch (error) {
	console.log(error);
}

// correct cards, incorrect cardID
try {
	getAllCardsForAList("data/cards.json", "qwsa221abcdefgh", callback);
} catch (error) {
	console.log(error);
}

// corrupted cards, correct cardID
try {
	getAllCardsForAList("data/corruptedData.json", "jwkh245", callback);
} catch (error) {
	console.log(error);
}

// missing cards arguments
try {
	getAllCardsForAList("jwkh245", callback);
} catch (error) {
	console.log(error);
}
