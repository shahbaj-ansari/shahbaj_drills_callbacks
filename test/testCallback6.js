const getBoardInfoCardInfoAndAllCards = require("../callback6.js");

// all 3 boards.json, cards.json and lists.json are correct, also passing correct boardID that is 'Thanos'
try {
	getBoardInfoCardInfoAndAllCards("data/boards.json", "data/cards.json", "data/lists.json", "Thanos", (err) => {
		console.log(err);
	});
} catch (error) {
	console.log(error);
}

// passing corrupted json data in place of boards.json , and pasing 'Thanoaas' instead of 'Thanos'
try {
	getBoardInfoCardInfoAndAllCards("data/corruptedData.json", "data/cards.json", "data/lists.json", "Thanoaas", (err) => {
		console.log(err);
	});
} catch (error) {
	console.log(error);
}
