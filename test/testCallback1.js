const getBoardInfo = require("../callback1.js");

const callback = function (err, data) {
	if (err) {
		console.log(err);
	} else {
		console.log(data);
	}
};

// Correct board, Correct boardID
try {
	getBoardInfo("data/boards.json", "mcu453ed", callback);
} catch (error) {
	console.log(error);
}

// Correct board, Incorrect boardID
try {
	getBoardInfo("data/boards.json", "mcu453ed1234", callback);
} catch (error) {
	console.log(error);
}

// CorruptedJSON Data, Correct boardID
try {
	getBoardInfo("data/corruptedData.json", "mcu453ed", callback);
} catch (error) {
	console.log(error);
}

// Not Passing sufficient parameters.
try {
	getBoardInfo(callback);
} catch (error) {
	console.log(error);
}
