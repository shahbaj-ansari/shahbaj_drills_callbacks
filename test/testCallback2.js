const getAllListsOfABoard = require("../callback2.js");

// common callback to all
const callback = function (err, data) {
	if (err) {
		console.log(err);
	} else {
		console.log(data);
	}
};

// correct lists, correct id
try {
	getAllListsOfABoard("data/lists.json", "mcu453ed", callback);
} catch (error) {
	console.log(error);
}

// correct lists, incorrect id
try {
	getAllListsOfABoard("data/lists.json", "mcu453edqwerty", callback);
} catch (error) {
	console.log(error);
}

// corrupted lists, correct id
try {
	getAllListsOfABoard("data/corruptedData.json", "mcu453ed", callback);
} catch (error) {
	console.log(error);
}

// by passing only callback, and leaving lists
try {
	getAllList(callback);
} catch (error) {
	console.log(error);
}
