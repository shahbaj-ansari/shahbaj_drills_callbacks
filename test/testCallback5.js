const getBoardInfoCardInfoAndAllCardsOfGivenLists=require('../callback5.js');


// all data boards, cards, lists are correct, boardID = Thanos and listName1 = Mind and listName2 = Space
try{
    getBoardInfoCardInfoAndAllCardsOfGivenLists('data/boards.json','data/cards.json','data/lists.json','Thanos','Mind','Space',err=>{
        console.log(err);
    });    
}catch(error){
    console.log(error);
}


// all data boards, cards, lists are correct, boardID = Thanooos (wrong) and listName1 = Mind123 (wrong) and listName2 = Space
try{
    getBoardInfoCardInfoAndAllCardsOfGivenLists('data/boards.json','data/cards.json','data/lists.json','Thanooos','Mind123','Space',err=>{
        console.log(err);
    });    
}catch(error){
    console.log(error);
}


// corrupted board and cards, but correct lists, boardID = Thanos and listName1 = Mind and listName2 = Space
try{
    getBoardInfoCardInfoAndAllCardsOfGivenLists('data/corruptedData.json','data/corruptedData.json','data/lists.json','Thanos','Mind','Space',err=>{
        console.log(err);
  });    
}catch(error){
    console.log(error);
}
