const getBoardInfoCardInfoAndAllCardsOfAList=require('../callback4.js');


// all data boards, cards, lists are correct, boardID = Thanos and listID = Mind are also correct
try{
    getBoardInfoCardInfoAndAllCardsOfAList('data/boards.json','data/cards.json','data/lists.json','Thanos','Mind',err=>{
        console.log(err);
    });
}catch(error){
    console.log(error);
}


// corrupted boards, and correct cards, lists, boardID = Thanos and listID = Mind are also correct
try{
    getBoardInfoCardInfoAndAllCardsOfAList('data/corruptedData.json','data/cards.json','data/lists.json','Thanos','Mind',err=>{
        console.log(err);
    });
}catch(error){
    console.log(error);
}

// all data boards, cards, lists are correct, boardID = Thanoooos (incorrect) and listID = Mind (correct)
try{
    getBoardInfoCardInfoAndAllCardsOfAList('data/boards.json','data/cards.json','data/lists.json','Thanoos','Mind',err=>{
        console.log(err);
    })
}catch(error){
    console.log(error);
}