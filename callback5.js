/*
	Problem 5: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind and Space lists simultaneously
*/
const fs = require("fs");
const path = require("path");
const getBoardInfo = require("./callback1");
const getAllListsOfABoard = require("./callback2");
const getAllCardsForAList = require("./callback3");

function getBoardInfoCardInfoAndAllCardsOfGivenLists(
	boardsPath,
	cardsPath,
	listsPath,
	boardName,
	listName1,
	listName2,
	callback
) {
	if (
		typeof boardsPath != "string" ||
		typeof cardsPath != "string" ||
		typeof listsPath != "string" ||
		typeof boardName != "string" ||
		typeof listName1 != "string" ||
		typeof listName2 != "string" ||
		typeof callback != "function"
	) {
		throw new Error(
			"Please Provide all 7 parameters in correct form : (boardsPath:string, cardsPath:string, listsPath:string, boardName:string, listName1:string, listName2:string, callback:function)"
		);
	} else {
		fs.readFile(path.join(__dirname, boardsPath), "utf-8", (err, data) => {
			if (err) {
				throw err;
			}

			try {
				const boards = JSON.parse(data);
				const boardNameObj = boards.find((currBoard) => currBoard.name == boardName);

				let idofboardName = "";
				if (boardNameObj != undefined) {
					idofboardName = boardNameObj.id;
				}

				getBoardInfo(boardsPath, idofboardName, function (err, data) {
					if (err) {
						console.log(err);
					} else {
						console.log(data);

						getAllListsOfABoard(listsPath, idofboardName, function (err, data) {
							if (err) {
								console.log(err);
							} else {
								console.log(data);

								fs.readFile(listsPath, "utf-8", (err, data) => {
									if (err) {
										throw err;
									}
									try {
										const lists = JSON.parse(data);

										function getID(name) {
											for (board_id in lists) {
												for (const list of lists[board_id]) {
													if (name == list.name) {
														return list.id;
													}
												}
											}
										}

										// getting ID of Name passed as listName ie: Mind and Space
										const idOfListName1 = getID(listName1);
										const idOfListName2 = getID(listName2);

										getAllCardsForAList(cardsPath, idOfListName1, (err, data) => {
											if (err) {
												console.log(err);
											} else {
												for (let i = 0; i < data.length; i++) {
													setTimeout(() => console.log(data[i]), i * 1000);
												}

												getAllCardsForAList(cardsPath, idOfListName2, (err, data) => {
													if (err) {
														console.log(err);
													} else {
														for (let i = 0; i < data.length; i++) {
															setTimeout(() => console.log(data[i]), i * 1000);
														}
													}
												});
											}
										});
									} catch (error) {
										throw error;
									}
								});
							}
						});
					}
				});
			} catch (error) {
				callback(error);
			}
		});
	}
}

module.exports = getBoardInfoCardInfoAndAllCardsOfGivenLists;
