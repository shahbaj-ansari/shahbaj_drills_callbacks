/*
Problem 1: Write a function that will return a particular board's information based on the boardID that
is passed from the given list of boards in boards.json and then pass control back to the code that called
it by using a callback function.
*/
const fs = require("fs");
const path = require("path");

function getBoardInfo(boardsPath, boardID, callback) {
	if (typeof boardsPath != "string" || typeof boardID != "string" || typeof callback != "function") {
		throw new Error(
			"Please Provide all 3 parameters in correct form : (boardsPath:string, boardID:string, callback:function)"
		);
	} else {
		setTimeout(() => {
			fs.readFile(path.join(__dirname, boardsPath), "utf-8", (err, data) => {
				if (err) {
					throw new Error(`Error Data Not Found`);
				}
				try {
					const boards = JSON.parse(data);
					const result = boards.find((currBoard) => currBoard.id == boardID);

					if (result == undefined) {
						throw new Error(`No board exists for given boardID : ${boardID}`);
					} else {
						callback(null, result);
					}
				} catch (error) {
					callback(error);
				}
			});
		}, 2 * 1000);
	}
}

module.exports = getBoardInfo;
