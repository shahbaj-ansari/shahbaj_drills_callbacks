/*
	Problem 6: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for all lists simultaneously
*/

const fs = require("fs");
const path = require("path");
const getBoardInfo = require("./callback1");
const getAllListsOfABoard = require("./callback2");
const getAllCardsForAList = require("./callback3");

function getBoardInfoCardInfoAndAllCards(boardsPath, cardsPath, listsPath, boardName, callback) {
	if (
		typeof boardsPath != "string" ||
		typeof cardsPath != "string" ||
		typeof listsPath != "string" ||
		typeof boardName != "string" ||
		typeof callback != "function"
	) {
		throw new Error(
			"Please Provide all 5 parameters in correct form : (boardsPath:string, cardsPath:string, listsPath:string, boardName:string, callback:function)"
		);
	} else {
		fs.readFile(path.join(__dirname, boardsPath), "utf-8", (err, data) => {
			if (err) {
				throw err;
			}

			try {
				const boards = JSON.parse(data);
				const boardNameObj = boards.find((currBoard) => currBoard.name == boardName);

				let idofboardName = ""; // get boardID of passed args.
				if (boardNameObj != undefined) {
					idofboardName = boardNameObj.id;
				}

				getBoardInfo(boardsPath, idofboardName, function (err, data) {
					if (err) {
						console.log(err);
					} else {
						console.log(data);

						getAllListsOfABoard(listsPath, idofboardName, function (err, data) {
							if (err) {
								console.log(err);
							} else {
								console.log(data);

								fs.readFile(listsPath, "utf-8", (err, data) => {
									if (err) {
										throw err;
									}
									try {
										const lists = JSON.parse(data);

										function getID(accum) {
											for (board_id in lists) {
												for (const list of lists[board_id]) {
													accum.push(list.id);
												}
											}
											return accum;
										}

										// getting IDs of All Lists
										const idOfLists = getID([]);

										let delay = 2000;

										for (let i = 0; i < idOfLists.length; i++) {
											getAllCardsForAList(cardsPath, idOfLists[i], (err, data) => {
												if (err) {
													console.log(err);
												} else {
													for (let j = 0; j < data.length; j++) {
														setTimeout(() => {
															console.log(data[j]);
														}, delay + i * 1000);
														delay = delay + 2000;
													}
												}
											});
										}
									} catch (error) {
										throw error;
									}
								});
							}
						});
					}
				});
			} catch (error) {
				callback(error);
			}
		});
	}
}

module.exports = getBoardInfoCardInfoAndAllCards;
